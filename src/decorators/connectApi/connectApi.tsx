import React from 'react'
import { Subtract } from 'utility-types'
import { ApiContext } from '../..'
import { ApiDefinition } from '../../types'

interface Props {}

export type ConnectedApiProps = {
  api: ApiDefinition
}

const connectApi = <P extends ConnectedApiProps>(
  Component: React.ComponentType<P>
): React.FC<Subtract<P, ConnectedApiProps> & Props> => (props: Props) => (
  <ApiContext.Consumer>
    {(api: ApiDefinition) => <Component {...(props as P)} api={api} />}
  </ApiContext.Consumer>
)

export default connectApi
