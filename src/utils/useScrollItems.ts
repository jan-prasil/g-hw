import React from 'react'
import isBottom from './isBottom'

const useScrollItems = (base: number, element: HTMLDivElement) => {
  const [showCount, setShowCount] = React.useState(base)
  React.useEffect(() => {
    document.addEventListener('scroll', handleScroll)
    return () => document.removeEventListener('scroll', handleScroll)
  })

  const handleScroll = () => {
    if (isBottom(element)) {
      setShowCount((currentShowCount) => currentShowCount + base)
    }
  }

  return { showCount, restart: () => setShowCount(base) }
}

export default useScrollItems
