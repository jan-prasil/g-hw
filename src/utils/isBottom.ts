const isBottom = (element: HTMLDivElement) =>
  element && element.getBoundingClientRect().bottom <= window.innerHeight + 20

export default isBottom
