export const getSearchParam = (searchString: string, param: string) => {
  const searchParams = new URLSearchParams(searchString)
  return searchParams.get(param)
}

export const setSearchParam = (param: string, value: string) => {
  const searchParams = new URLSearchParams(window.location.search)
  if (!value) {
    searchParams.delete(param)
  } else {
    searchParams.set(param, value)
  }
  const pageUrl = `?${searchParams.toString()}`
  window.history.pushState('', '', pageUrl)
}
