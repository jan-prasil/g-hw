import { ForexDataModel, ForexDetailModel } from '../types'

class ForexData {
  constructor(data: ForexDataModel) {
    this.data = { ...data }
  }

  data: ForexDataModel

  getByCurrency(currency: string): Array<ForexDetailModel> {
    return this.data.fx
      .filter(
        (item) =>
          !!item.currency.trim() &&
          item.currency.toLowerCase().indexOf(currency.toLowerCase()) === 0
      )
      .sort((a, b) => (a.currency > b.currency ? 1 : -1))
  }
}

export default ForexData
