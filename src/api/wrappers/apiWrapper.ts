import { ApiMethods } from '../../types'

const createApiWrapper = (): ApiMethods => ({
  fetch: <T>(path: string) =>
    new Promise<T>((resolve) =>
      fetch(path).then((result) => result.json().then((json) => resolve(json)))
    ),
})

export default createApiWrapper
