import { ApiDefinition, ApiMethods, ForexDataModel } from '../types'

const createApi = ({ fetch }: ApiMethods): ApiDefinition => ({
  fetchForexData: () => fetch<ForexDataModel>('/fx.json'),
})

export default createApi
