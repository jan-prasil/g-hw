import { Spin } from 'antd'
import React from 'react'
import styles from './Loader.module.css'

const Loader = () => (
  <div className={styles.loader}>
    <h1>...LOADING...</h1>
    <Spin />
  </div>
)

export default Loader
