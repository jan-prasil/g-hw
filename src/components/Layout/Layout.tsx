import { Col, Layout, Row } from 'antd'
import { Content, Footer, Header } from 'antd/lib/layout/layout'
import { Logo, SearchBar } from '..'
import React from 'react'
import styles from './Layout.module.css'

interface Props {
  children: React.ReactNode
  searchTerm: string
  onSearchChange: (term: string) => void
}

const LayoutComponent = ({ children, searchTerm, onSearchChange }: Props) => (
  <Layout>
    <Header>
      <Row>
        <Col xs={12} md={6}>
          <Logo />
        </Col>
        <Col xs={12} md={{ offset: 12, span: 6 }}>
          <SearchBar searchTerm={searchTerm} onChange={onSearchChange} />
        </Col>
      </Row>
    </Header>
    <Content className={styles.content}>{children}</Content>
    <Footer className={styles.footer}>GeorgeFX HomeWork 🚀</Footer>
  </Layout>
)

export default LayoutComponent
