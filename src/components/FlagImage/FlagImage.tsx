import React from 'react'
import { ForexDetailModel } from '../../types'
import styles from './FlagImage.module.css'

const images = require.context('../../assets/flags')

interface Props {
  item: ForexDetailModel
}

const FlagImage = ({ item }: Props) => {
  try {
    return (
      <img
        alt={item.currency}
        src={images(`./${item.currency.substring(0, 2).toLowerCase()}.png`).default}
      />
    )
  } catch (e) {
    return <div className={styles.alternativePreview}>❓</div>
  }
}

export default FlagImage
