import { Col, Row } from 'antd'
import { FlagImage } from '..'
import { ForexDetailModel } from '../../types'
import React from 'react'
import styles from './ExchangeRow.module.css'
import TransferCalculator from '../TransferCalculator/TransferCalculator'

interface Props {
  baseCurrency: string
  item: ForexDetailModel
}

export const ExchangeRow = ({ baseCurrency, item }: Props) => {
  const buy = item.exchangeRate?.buy
  const sell = item.exchangeRate?.sell

  return (
    <Col xs={24} className={styles.row}>
      <FlagImage item={item} />
      <Row className={styles.innerRow}>
        <Col xs={24} md={2}>
          <strong className={styles.title}>{item.currency}</strong>
        </Col>
        <Col xs={24} md={8}>
          {item.nameI18N && <i>({item.nameI18N})</i>}
        </Col>
        <Col xs={24} md={{ offset: 2, span: 12 }} className={styles.textRight}>
          <TransferCalculator
            precision={item.precision}
            type="sell"
            firstValue={{ currency: baseCurrency, value: 1 }}
            secondValue={{ currency: item.currency, value: buy }}
          />
          <TransferCalculator
            precision={item.precision}
            type="buy"
            firstValue={{ currency: item.currency, value: sell }}
            secondValue={{ currency: baseCurrency, value: sell }}
          />
        </Col>
      </Row>
    </Col>
  )
}

export default ExchangeRow
