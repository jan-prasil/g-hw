import { Input } from 'antd'
import React from 'react'

interface Props {
  searchTerm: string
  onChange: (term: string) => void
}

const SearchBar = ({ searchTerm, onChange }: Props) => {
  const onInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    onChange && onChange(e.target.value)
  }

  return <Input placeholder="Search by country" onChange={onInputChange} value={searchTerm} />
}

export default SearchBar
