import Text from 'antd/lib/typography/Text'
import React from 'react'
import styles from './TransferCalculator.module.css'

interface Props {
  type: 'sell' | 'buy'
  firstValue: {
    value?: number
    currency?: string
  }
  secondValue: {
    value?: number
    currency?: string
  }
  precision: number
}

const messages = {
  title: {
    sell: 'Sell ',
    buy: 'Buy ',
  },
  unknown: {
    buy: 'Unknown buy information',
    sell: 'Unknown sell information',
  },
}

const TransferCalculator = ({ type, firstValue, secondValue, precision }: Props) => {
  const [base, setBase] = React.useState(firstValue.value)
  const computedSecondValue =
    base &&
    secondValue.value &&
    base * (type === 'sell' ? secondValue.value : 1 / secondValue.value)

  return firstValue.value && secondValue.value ? (
    <div className={styles.transferCalculator}>
      {messages.title[type]}
      <strong>
        <Text
          editable={{
            autoSize: { maxRows: 1 },
            onChange: (value) => setBase(parseFloat(value) || base),
            maxLength: 10,
          }}>
          {base?.toFixed(precision)}
        </Text>
        {'  '}
        {firstValue.currency}
        <span> ≈ </span>
        {computedSecondValue?.toFixed(precision)} {secondValue.currency}
      </strong>
    </div>
  ) : (
    <div>{messages.unknown[type]}</div>
  )
}
export default TransferCalculator
