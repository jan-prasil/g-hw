import React from 'react'
import styles from './Logo.module.css'
import Text from 'antd/lib/typography/Text'

const Logo = () => <Text className={styles.logo}>George[𝑓𝒙] 🌍</Text>

export default Logo
