export interface Rate {
  buy: number
  middle: number
  sell: number
  indicator: number
  lastModified: string
}

export interface ForexDetailModel {
  currency: string
  precision: number
  nameI18N: string
  exchangeRate?: Rate
  banknoteRate?: Rate
  flags: Array<'provided'>
}

export interface ForexDataModel {
  institute: number
  lastUpdated: string
  comparisonDate: string
  baseCurrency: string
  fx: Array<ForexDetailModel>
}

export interface ApiDefinition {
  fetchForexData: () => Promise<ForexDataModel>
}

export interface ApiMethods {
  fetch: <T>(path: string) => Promise<T>
}
