import apiWrapper from './api/wrappers/apiWrapper'
import App from './App'
import createApi from './api/createApi'
import React from 'react'
import ReactDOM from 'react-dom'
import reportWebVitals from './reportWebVitals'

export const api = createApi(apiWrapper())
export const ApiContext = React.createContext(api)

ReactDOM.render(
  <React.StrictMode>
    <ApiContext.Provider value={api}>
      <App />
    </ApiContext.Provider>
  </React.StrictMode>,
  document.getElementById('root')
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals(console.log)
