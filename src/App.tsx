import React from 'react'

import 'antd/dist/antd.css'
import { CurrencyList } from './containers'
import { Layout } from './components'
import { getSearchParam, setSearchParam } from './utils'

const SEARCH_PARAM = 'c'

const App = () => {
  const [searchTerm, setSearchTerm] = React.useState('')
  React.useEffect(() => {
    setSearchTerm(getSearchParam(window.location.search, SEARCH_PARAM) || '')
  }, [])

  React.useEffect(() => {
    setSearchParam(SEARCH_PARAM, searchTerm)
  }, [searchTerm])

  return (
    <Layout searchTerm={searchTerm} onSearchChange={setSearchTerm}>
      <CurrencyList searchTerm={searchTerm} />
    </Layout>
  )
}

export default App
