import { Col, Row } from 'antd'
import { ExchangeRow, Loader } from '../../components'
import { ForexDetailModel } from '../../types'
import React from 'react'
import styles from './CurrencyList.module.css'

interface Props {
  baseCurrency: string
  data?: Array<ForexDetailModel>
  pending: boolean
  rowRef: React.RefObject<HTMLDivElement>
  showBottomLoader: boolean
}

const CurrencyListComponent = ({
  baseCurrency,
  data,
  pending,
  rowRef,
  showBottomLoader,
}: Props) => (
  <div ref={rowRef}>
    {pending || !data ? (
      <Loader />
    ) : (
      <Row>
        <Col xs={24} sm={{ span: 20, offset: 2 }} xl={{ span: 12, offset: 6 }}>
          {data.length === 0 && (
            <div className={styles.notFound}>
              ❗️ Unfurtunately, currency you look for couldn't been found 🥺
              <br />
              Try it again, please 🤞 💪
            </div>
          )}
          <Row>
            {data.map((item, key) => (
              <ExchangeRow baseCurrency={baseCurrency} item={item} key={item.currency + key} />
            ))}
          </Row>
          {showBottomLoader && <Loader />}
        </Col>
      </Row>
    )}
  </div>
)

export default CurrencyListComponent
