import { connectApi } from '../../decorators'
import { ConnectedApiProps } from '../../decorators/connectApi/connectApi'
import { useScrollItems } from '../../utils'
import CurrencyListComponent from './CurrencyList.comp'
import ForexData from '../../models/ForexData'
import React from 'react'

interface BaseProps {
  searchTerm: string
}

type Props = BaseProps & ConnectedApiProps

const CurrencyListContainer: React.FC<Props> = ({ api, searchTerm }: Props) => {
  const rowRef = React.useRef() as React.MutableRefObject<HTMLDivElement>
  const [data, setData] = React.useState<ForexData>()
  const [pending, setPending] = React.useState(true)
  const { restart, showCount } = useScrollItems(10, rowRef.current)

  React.useEffect(() => {
    api
      .fetchForexData()
      .then((data) => setData(new ForexData(data)))
      .finally(() => setPending(false))
  }, [api])

  React.useEffect(restart, [searchTerm])

  const filteredData =
    React.useMemo(() => data?.getByCurrency(searchTerm), [data, searchTerm]) || []

  return (
    <CurrencyListComponent
      baseCurrency={data?.data.baseCurrency || ''}
      data={filteredData.slice(0, showCount)}
      pending={pending}
      showBottomLoader={showCount < filteredData.length}
      rowRef={rowRef}
    />
  )
}

export default connectApi(CurrencyListContainer)
